govc ls -k /Datacenter/host/Cluster

install web server
```bash
$ sudo dnf install httpd -y
```

secret
https://console.redhat.com/openshift/install/pull-secret



### scanning IP in vlan
```bash
$ sudo dnf install nmap -y
```
```bash
$ nmap -sP 192.168.178.0/24
```


### installing cluster
```bash
$ openshift-install create cluster --dir myinst/ --log-level debug > /tmp/log-install.log 2>&1 &
```

### extract binary
```bash
tar xzvf openshift-intall-linux....
```


### base cluster
```bash
$ ssh -i ~/.ssh/c-id-rsa core@10.10.0.9 "sudo oc --kubeconfig=/etc/kubernetes/kubeconfig get pods -A"
```
[]

### updating NTP info
https://docs.openshift.com/container-platform/4.15/installing/install_config/installing-customizing.html



### ODF tool 
```bash
$ oc patch OCSInitialization ocsinit -n openshift-storage --type json --patch  '[{ "op": "replace", "path": "/spec/enableCephTools", "value": true }]'
```


### Observability
https://docs.openshift.com/container-platform/4.15/observability/monitoring/monitoring-overview.html

### kibana example
https://console-openshift-console.apps.c2.sandbox2619.opentlc.com


### api resource
https://docs.openshift.com/container-platform/4.15/cli_reference/openshift_cli/developer-cli-commands.html


### ad hoc command
```bash
$ oc exec patty-7768fcbfcf-j5h26 -- curl -sIXGET selma:8080
```

### deploying postgres on OCP
https://gitlab.com/rcardona/mydocs/-/blob/master/databases/postgresql/psql-in-ocp.md

### checking on kvm
```bash
$ oc get nodes -o json|jq '.items[]|{"name": .metadata.name, "kvm": .status.allocatable["devices.kubevirt.io/kvm"]}'
```


### virtctl 
```bash
oc get ConsoleCLIDownload virtctl-clidownloads-kubevirt-hyperconverged -o yaml
```

### expose vm to the outside world
```bash
$ virtctl expose virtualmachine windows-vm-1 --name windows-vm-1-svc --port 12303 --target-port 3389 --type NodePort
```