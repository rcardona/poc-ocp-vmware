# Installer Provided Infrastructure

- [Standard Prerequisites](https://docs.openshift.com/container-platform/4.15/installing/installing_vsphere/ipi/installing-vsphere-installer-provisioned.html#prerequisites_installing-vsphere-installer-provisioned_installing-vsphere-installer-provisioned) 

- Firewall ports acces
   
   - 443 : bastion host --> vCenter API
   - 443 : bastion host --> ESX hosts
   - 443 : Openshift Nodes --> vCenter API
   - 123 : Openshift Nodes --> NTP Server(s)


### Use Cases $`\textcolor{red}{\text{(To be updated)}}`$

- [x] Automated deployment with IPI, [DOCS](https://docs.openshift.com/container-platform/4.15/installing/installing_vsphere/ipi/installing-vsphere-installer-provisioned.html#installing-vsphere-installer-provisioned)

- [x] Enterprise storage, Openshift Data Foundation deployment, [DOCS](https://access.redhat.com/documentation/en-us/red_hat_openshift_data_foundation/4.15/html-single/deploying_and_managing_openshift_data_foundation_using_red_hat_openstack_platform/index)

- [x] Cluster Observability , [USE CASE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/monitoring/configure-custom-alerting.md), [DOCS](https://docs.openshift.com/container-platform/4.15/monitoring/managing-alerts.html#managing-alerting-rules-for-user-defined-projects_managing-alerts)

- [x] Monitoring and Logging, [DOCS](https://docs.openshift.com/container-platform/4.15/monitoring/monitoring-overview.html)

- [x] Openshift Cluster AutoScaling, [USE CASE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/autoscaling/cluster-autoscaling.md), [DOCS](https://docs.openshift.com/container-platform/4.15/machine_management/applying-autoscaling.html)

- [x] Application Multitenancy, Role Based Access Control, [USE CASE 0](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/identity-management/htpasswd/configuring-identity-provider.md),
 [USE CASE 1](https://gitlab.com/rcardona/ocp4-apps-examples/-/blob/master/odf/cephfs-app.md)

- [x] Network Policies [USE CASE 0](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/networking/network-policies/configuration.md?ref_type=heads)


- [x] Wish list: Openshift AI [HERE](https://gitlab.com/rcardona/ocp-ai/-/tree/main/intro?ref_type=heads), Openshift Virtualization, [cluster](https://console-openshift-console.apps.c4.sandbox2989.opentlc.com)

### 0 - (optional) Bastion Host
To have a dedicated host to run the installation is not a requirement, and has been added here to add clarity and centralize access to OCP cluster administrators. In this case it has been suggested as follow,

- 0.1 - RHEL VM (version > RHEL v8) with minimal server capabilities ready deployed with access to the vCenter API

- 0.2 - Configure DHCP server on bastion host [EXAMPLE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/utilities/dhcp-server.md?ref_type=heads)

### 1 - Getting Artifacts

- 1.1 - Install govc utility linux x86_64 (optional, but very usuful for testing purposes)
   
   ```bash
   $ wget https://github.com/vmware/govmomi/releases/download/v0.37.1/govc_Linux_x86_64.tar.gz
   ```
   ```bash
   $ echo -e  "
   export GOVC_URL='https://vcenter.example.com'
   export VC_GOVC='vcenter.exmaple.com'
   export GOVC_USERNAME='administrator@vsphere.local'
   export GOVC_PASSWORD='MYPASSWORD' | tee ~/govc_env.sh
   ```
   ```bash
   $ wget https://vcenter.example.com/certs/download.zip --no-check-certificate
   ```
   ```bash
   $ unzip download.zip
   ```
   ```bash
   $ cp certs/lin/* /etc/pki/ca-trust/source/anchors
   ```
   ```bash
   $ update-ca-trust extract
   ```
   ```bash
   $ govc ls -k 
   ```

- 1.2 - Create SSH key to access hosts
   ```bash
   $ ssh-keygen -t ed25519 -N '' -f ~/.ssh/id_rsa
   ```

- 1.3 - Getting installer binaries
   ```bash
   $ wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/4.15.19/openshift-install-linux.tar.gz
   ```
   ```bash
   $ $ wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-client-linux.tar.gz
   ```
   ```bash
   $ $ wget https://mirror.openshift.com/pub/openshift-v4/x86_64/dependencies/rhcos/4.15/latest/rhcos-vmware.x86_64.ova
   ```

### 2 - Previous configuration

- 2.0 Lets create an identity provider

   [USE CASE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/identity-management/htpasswd/configuring-identity-provider.md)

    > $ htpasswd -c -B -b users.htpasswd admin admin
    >
    > $ for MYUSER in admin1 admin2 dev1 dev2 dev3 dev4 dev5; do htpasswd -B -b users.htpasswd $MYUSER $MYUSER; done
    >

### 3 - Non secure app

    echo -n 'apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: nginx-deployment
      labels:
        app: nginx
    spec:
      replicas: 3
      selector:
        matchLabels:
          app: nginx
      template:
        metadata:
          labels:
            app: nginx
        spec:
          containers:
          - name: nginx
            image: nginx:1.14.2
            ports:
            - containerPort: 80' | tee deployment-wild-nginx.yaml

   > oc new-project testX
   >
   > oc apply -f deployment-wild-nginx.yaml
   >

---
